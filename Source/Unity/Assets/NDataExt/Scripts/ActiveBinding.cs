﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ActiveBinding.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.NDataExt
{
    using UnityEngine;

    public class ActiveBinding : NguiBooleanBinding
    {
        #region Fields

        public GameObject Target;

        #endregion

        #region Methods

        protected override void ApplyNewValue(bool newValue)
        {
            // NOTE(co): Have to check, game object might be already destroyed.
            if (this == null)
            {
                return;
            }

            GameObject target = this.Target != null ? this.Target : this.gameObject;
            target.SetActive(newValue);
        }

        #endregion
    }
}