﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CreateAnimationWindow.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using System.Globalization;
using System.Linq;

using ToyHunt.Unity.Visual;

using UnityEditor;

using UnityEngine;

public class CreateAnimationWindow : EditorWindow
{
    #region Fields

    private Texture2D firstAnimationFrame;

    private int frameCount = 8;

    #endregion

    #region Public Methods and Operators

    [MenuItem("ToyHunt/Create Animation")]
    public static void OpenCreateAnimationWindow()
    {
        GetWindow<CreateAnimationWindow>(false, "Create Animation", true).Show();
    }

    #endregion

    #region Methods

    private void OnGUI()
    {
        this.firstAnimationFrame =
            (Texture2D)
            EditorGUILayout.ObjectField("First animation frame", this.firstAnimationFrame, typeof(Texture2D), false);
        this.frameCount = EditorGUILayout.IntField("Frame count", this.frameCount);

        if (GUILayout.Button("Create Animation"))
        {
            var path = AssetDatabase.GetAssetPath(this.firstAnimationFrame);

            if (this.frameCount > 1)
            {
                var newAnimation =
                    new GameObject(this.firstAnimationFrame.name.Substring(0, this.firstAnimationFrame.name.Length - 3));
                var animationData = newAnimation.AddComponent<SpriteAnimationData>();

                animationData.FrameDuration = 0.1f;

                var spriteList = new List<Sprite>();
                for (int i = 0; i < this.frameCount; ++i)
                {
                    var nextSprite =
                        AssetDatabase.LoadAllAssetsAtPath(
                            path.Replace("1", (i + 1).ToString(CultureInfo.InvariantCulture)))
                                     .OfType<Sprite>()
                                     .FirstOrDefault();
                    spriteList.Add(nextSprite);
                }

                animationData.Sprites = spriteList.ToArray();
            }
            else
            {
                var newAnimation = new GameObject(this.firstAnimationFrame.name);
                var animationData = newAnimation.AddComponent<SpriteAnimationData>();

                animationData.FrameDuration = 0.1f;

                var nextSprite = AssetDatabase.LoadAllAssetsAtPath(path).OfType<Sprite>().FirstOrDefault();
                animationData.Sprites = new[] { nextSprite };
            }
        }
    }

    #endregion
}