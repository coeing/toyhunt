﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ToyHuntGameBehaviour.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Unity.Game
{
    using Slash.GameBase;
    using Slash.Unity.Common.Configuration;
    using Slash.Unity.Common.Core;
    using Slash.Unity.Common.Scenes;

    using ToyHunt.Core.Logic;
    using ToyHunt.Unity.UI.InGameMenu;

    using UnityEngine;

    using XInputDotNetPure;

    using Event = Slash.GameBase.Events.Event;

    public class ToyHuntGameBehaviour : MonoBehaviour
    {
        #region Fields

        public InGameMenu InGameMenu;

        private GameBehaviour gameBehaviour;

        private SceneManager sceneManager;

        #endregion

        #region Public Methods and Operators

        public void StartNewGame()
        {
            var toyHuntGame = new Game();
            toyHuntGame.BlueprintManager = FindObjectOfType<GameConfigurationBehaviour>().BlueprintManager;
            this.gameBehaviour.StartGame(toyHuntGame);

            this.gameBehaviour.Game.EventManager.RegisterListener(ToyHuntEvent.GameOver, this.OnGameOver);
        }

        #endregion

        #region Methods

        private void Awake()
        {
            this.sceneManager = SceneManager.Instance;
            this.gameBehaviour = this.GetComponent<GameBehaviour>();
        }

        private void OnDisable()
        {
            this.InGameMenu.PlayPressed -= this.OnPlayPressed;
        }

        private void OnEnable()
        {
            this.InGameMenu.PlayPressed += this.OnPlayPressed;
        }

        private void OnGameOver(Event e)
        {
            this.gameBehaviour.Game = null;
        }

        private void OnPlayPressed()
        {
            this.InGameMenu.gameObject.SetActive(false);
            this.StartNewGame();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                this.sceneManager.ChangeScene("MainMenu");
            }
        }

        #endregion
    }
}