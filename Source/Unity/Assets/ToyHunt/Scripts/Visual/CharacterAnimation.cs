﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CharacterAnimation.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Unity.Visual
{
    using System.Linq;

    using Slash.GameBase.Events;
    using Slash.Unity.Common.Core;

    using ToyHunt.Core.Logic;
    using ToyHunt.Logic.EventData;
    using ToyHunt.Unity.Rendering;

    public class CharacterAnimation : GameEventBehaviour
    {
        #region Fields

        public SpriteAnimationData[] Animations;

        public CharacterMovement CharacterMovement;

        public string CharacterName = "girl";

        public float NormalWalkSpeed = 400.0f;

        public RotationFromDirection RotationFromDirection;

        public SpriteAnimation SpriteAnimation;

        public float WalkSpeedThreshold = 50.0f;

        private bool dashing;

        private EntityConfigurationBehaviour entityConfigurationBehaviour;

        private int itemCount;

        #endregion

        #region Public Methods and Operators

        public SpriteAnimationData GetAnimation(string animationName)
        {
            return this.Animations.FirstOrDefault(anim => anim.gameObject.name.StartsWith(animationName));
        }

        #endregion

        #region Methods

        protected override void Awake()
        {
            base.Awake();

            this.entityConfigurationBehaviour = this.GetComponent<EntityConfigurationBehaviour>();
        }

        protected override void RegisterListeners()
        {
            this.RegisterListener(ToyHuntEvent.ItemPickedUp, this.OnItemPickedUp);
            this.RegisterListener(ToyHuntEvent.ItemDropped, this.OnItemDropped);
            this.RegisterListener(ToyHuntEvent.DashCollision, this.OnDashCollision);

            this.SpriteAnimation.AnimationFinished += this.OnAnimationFinished;
        }

        private void OnAnimationFinished(string animationName)
        {
            this.dashing = false;
        }

        private void OnDashCollision(Event e)
        {
            var data = (DashCollisionData)e.EventData;

            if (data.DashedEntityId != this.entityConfigurationBehaviour.EntityId)
            {
                return;
            }

            // Play dash animation.
            var animationName = string.Format("{0}_dashed", this.CharacterName);
            var newAnimation = this.GetAnimation(animationName);
            this.SpriteAnimation.PlayAnimation(newAnimation);
            this.dashing = true;
        }

        private void OnItemDropped(Event e)
        {
            var itemData = (ItemDroppedData)e.EventData;
            if (itemData.PawnEntityId != this.entityConfigurationBehaviour.EntityId)
            {
                return;
            }

            --this.itemCount;
        }

        private void OnItemPickedUp(Event e)
        {
            PawnItemData itemData = (PawnItemData)e.EventData;
            if (itemData.PawnEntityId != this.entityConfigurationBehaviour.EntityId)
            {
                return;
            }

            ++this.itemCount;
        }

        private void Update()
        {
            if (this.dashing)
            {
                // Don't change animation while dashing.
                return;
            }

            // Compute speed factor.
            float speedFactor;
            string animationNameSpeed;
            string animationOrientation = string.Empty;

            // Compose animation name.
            if (this.CharacterMovement.Speed > this.WalkSpeedThreshold)
            {
                speedFactor = this.CharacterMovement.Speed / this.NormalWalkSpeed;
                animationNameSpeed = "walk";
            }
            else
            {
                speedFactor = 1.0f;
                animationNameSpeed = "stand";
            }

            var animationNameBackpack = this.itemCount > 0 ? (this.itemCount > 1 ? "big" : "medium") : "small";

            switch (this.RotationFromDirection.CurrentOrientation)
            {
                case Orientation.East:
                case Orientation.West:
                    animationOrientation = "s";
                    break;

                case Orientation.North:
                    animationOrientation = "b";
                    break;

                case Orientation.South:
                    animationOrientation = "f";
                    break;
            }

            var animationName = string.Format(
                "{0}_{1}_{2}_bp_{3}",
                this.CharacterName,
                animationOrientation,
                animationNameSpeed,
                animationNameBackpack);
            var newAnimation = this.GetAnimation(animationName);

            this.SpriteAnimation.PlayAnimation(newAnimation, speedFactor);
        }

        #endregion
    }
}