﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HighlightSpriteAlpha.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Unity.Visual
{
    using UnityEngine;

    public class HighlightSpriteAlpha : MonoBehaviour
    {
        #region Fields

        public float FadeSpeed = 1.0f;

        public float MinAlpha = 0.5f;

        public SpriteRenderer Target;

        private FadeState fadeState;

        #endregion

        #region Enums

        private enum FadeState
        {
            None,

            FadeOut,

            FadeIn,

            FadeInAndStop
        }

        #endregion

        #region Public Methods and Operators

        public void StartHighlight()
        {
            if (this.fadeState == FadeState.FadeIn || this.fadeState == FadeState.FadeOut)
            {
                return;
            }

            this.fadeState = FadeState.FadeOut;
        }

        public void StopHighlight()
        {
            if (this.fadeState == FadeState.None || this.fadeState == FadeState.FadeInAndStop)
            {
                return;
            }

            this.fadeState = FadeState.FadeInAndStop;
        }

        #endregion

        #region Methods

        private void Awake()
        {
            if (this.Target == null)
            {
                this.Target = this.GetComponent<SpriteRenderer>();
            }
        }

        private void Update()
        {
            var oldColor = this.Target.color;
            float newAlpha = oldColor.a;

            switch (this.fadeState)
            {
                case FadeState.FadeOut:
                    newAlpha = oldColor.a - this.FadeSpeed * Time.deltaTime;

                    if (newAlpha <= this.MinAlpha)
                    {
                        this.fadeState = FadeState.FadeIn;
                    }
                    break;

                case FadeState.FadeIn:
                    newAlpha = oldColor.a + this.FadeSpeed * Time.deltaTime;

                    if (newAlpha >= 1.0f)
                    {
                        this.fadeState = FadeState.FadeOut;
                    }
                    break;

                case FadeState.FadeInAndStop:
                    newAlpha = oldColor.a + this.FadeSpeed * Time.deltaTime;

                    if (newAlpha >= 1.0f)
                    {
                        this.fadeState = FadeState.None;
                    }
                    break;
            }

            this.Target.color = new Color(oldColor.r, oldColor.g, oldColor.b, newAlpha);
        }

        #endregion
    }
}