﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SpriteAnimationData.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Unity.Visual
{
    using UnityEngine;

    public class SpriteAnimationData : MonoBehaviour
    {
        #region Fields

        public float FrameDuration;

        public Sprite[] Sprites;

        #endregion
    }
}