﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ActiveTargetZone.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Unity.Visual
{
    using Slash.GameBase.Events;
    using Slash.Unity.Common.Core;

    using ToyHunt.Core.Logic;
    using ToyHunt.Logic.EventData;

    public class ActiveTargetZone : GameEventBehaviour
    {
        #region Fields

        public HighlightSpriteAlpha HighlightSpriteAlpha;

        private EntityConfigurationBehaviour entityConfigurationBehaviour;

        #endregion

        #region Methods

        protected override void Awake()
        {
            base.Awake();

            this.entityConfigurationBehaviour = this.GetComponent<EntityConfigurationBehaviour>();
        }

        protected override void RegisterListeners()
        {
            base.RegisterListeners();

            this.RegisterListener(ToyHuntEvent.TargetZonePawnCountChanged, this.OnTargetZonePawnCountChanged);
        }

        private void OnTargetZonePawnCountChanged(Event e)
        {
            var data = (TargetZonePawnCountChangedData)e.EventData;

            if (data.TargetZoneEntityId != this.entityConfigurationBehaviour.EntityId)
            {
                return;
            }

            if (data.PawnCount == 0)
            {
                this.HighlightSpriteAlpha.StopHighlight();
            }
            else
            {
                this.HighlightSpriteAlpha.StartHighlight();
            }
        }

        #endregion
    }
}