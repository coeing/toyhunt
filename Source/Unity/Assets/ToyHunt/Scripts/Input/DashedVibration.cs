﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DashedVibration.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Unity.Input
{
    using Slash.Unity.Common.Core;

    using ToyHunt.Core.Logic;
    using ToyHunt.Logic.EventData;

    using UnityEngine;

    using Event = Slash.GameBase.Events.Event;

    public class DashedVibration : GameEventBehaviour
    {
        #region Fields

        public GamePadVibration GamePadVibration;

        public float MaximumForce = 1000.0f;

        public float VibrationDuration = 0.5f;

        private EntityConfigurationBehaviour entityConfigurationBehaviour;

        private float remainingVibrationDuration;

        #endregion

        #region Methods

        protected override void Awake()
        {
            base.Awake();

            this.entityConfigurationBehaviour = this.GetComponent<EntityConfigurationBehaviour>();
        }

        protected override void RegisterListeners()
        {
            this.RegisterListener(ToyHuntEvent.DashCollision, this.OnDashCollision);
        }

        private void EnableVibration(bool enable, float force)
        {
            if (enable)
            {
                float vibrationFactor = force / this.MaximumForce;
                this.GamePadVibration.SetLeftMotor(vibrationFactor);
                this.remainingVibrationDuration = this.VibrationDuration * vibrationFactor;
            }
            else
            {
                this.GamePadVibration.SetLeftMotor(0.0f);
            }
        }

        private void OnDashCollision(Event e)
        {
            DashCollisionData eventData = (DashCollisionData)e.EventData;
            if (eventData.DashedEntityId == this.entityConfigurationBehaviour.EntityId
                || eventData.DashedEntityId == 0
                && eventData.DashingEntityId == this.entityConfigurationBehaviour.EntityId)
            {
                // Set vibration on dash.
                this.EnableVibration(true, eventData.Force);
            }
        }

        /// <summary>
        ///   Per frame update.
        /// </summary>
        private void Update()
        {
            if (this.remainingVibrationDuration > 0.0f)
            {
                this.remainingVibrationDuration -= Time.deltaTime;
                if (this.remainingVibrationDuration <= 0.0f)
                {
                    this.EnableVibration(false, 0);
                }
            }
        }

        #endregion
    }
}