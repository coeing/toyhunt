﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GamePadVibration.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Unity.Input
{
    using Slash.Collections.AttributeTables;
    using Slash.Unity.Common.Core;

    using ToyHunt.Logic.Components;

    using UnityEngine;

    using XInputDotNetPure;

    public class GamePadVibration : MonoBehaviour
    {
        #region Fields

        public float LeftMotor;

        public float RightMotor;

        private EntityConfigurationBehaviour entityConfigurationBehaviour;

        #endregion

        #region Public Methods and Operators

        public void SetLeftMotor(float value)
        {
            this.LeftMotor = value;
            this.UpdateVibration();
        }

        public void SetRightMotor(float value)
        {
            this.RightMotor = value;
            this.UpdateVibration();
        }

        #endregion

        #region Methods

        private void Awake()
        {
            this.entityConfigurationBehaviour = this.GetComponent<EntityConfigurationBehaviour>();
        }

        private void UpdateVibration()
        {
            PlayerIndex playerIndex =
                (PlayerIndex)
                this.entityConfigurationBehaviour.Configuration.GetIntOrDefault(
                    PlayerOwnerComponent.AttributePlayerOwner, PlayerOwnerComponent.DefaultPlayerOwner);
            GamePad.SetVibration(playerIndex, this.LeftMotor, this.RightMotor);
        }

        #endregion
    }
}