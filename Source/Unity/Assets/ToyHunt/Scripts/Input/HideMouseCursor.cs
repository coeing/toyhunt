﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HideMouseCursor.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Unity.Input
{
    using UnityEngine;

    public class HideMouseCursor : MonoBehaviour
    {
        #region Methods

        /// <summary>
        ///   Called before first Update call.
        /// </summary>
        private void Start()
        {
            Screen.showCursor = false;
        }

        #endregion
    }
}