﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DashVibration.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Unity.Input
{
    using Slash.Math.Utils;
    using Slash.Unity.Common.Core;

    public class DashVibration : GameEventBehaviour
    {
        #region Fields

        public CharacterDash Dash;

        public GamePadVibration GamePadVibration;

        public CharacterMovement Movement;

        #endregion

        #region Methods

        /// <summary>
        ///   Per frame update.
        /// </summary>
        private void Update()
        {
            float dashMaxExtraSpeed = this.Dash.DashForce;
            float dashExtraSpeed = this.Movement.Speed - this.Movement.MaxSpeed;
            float vibrationFactor = MathUtils.Clamp(dashExtraSpeed / dashMaxExtraSpeed, 0, 1);
            this.GamePadVibration.SetRightMotor(vibrationFactor);
        }

        #endregion
    }
}