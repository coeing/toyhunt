﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CharacterInput.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Unity.Input
{
    using Slash.Unity.Common.Core;

    using ToyHunt.Core.Logic;
    using ToyHunt.Logic.Data;

    using UnityEngine;

    using XInputDotNetPure;

    using Event = Slash.GameBase.Events.Event;

    public class CharacterInput : GameEventBehaviour
    {
        #region Fields

        public int InputIndex;

        private MatchState CurrentMatchState;

        #endregion

        #region Public Properties

        public bool DashPressed
        {
            get
            {
                if (!this.AcceptsInput)
                {
                    return false;
                }

                var buttonState = (this.InputIndex % 2 == 0)
                                      ? this.GamePadState.Buttons.LeftShoulder
                                      : this.GamePadState.Buttons.RightShoulder;
                if (Input.GetButton("Dash_" + this.InputIndex) || buttonState == ButtonState.Pressed)
                {
                    return true;
                }

                return false;
            }
        }

        public Vector2 Direction
        {
            get
            {
                if (!this.AcceptsInput)
                {
                    return Vector2.zero;
                }

                var keyboardX = Input.GetAxis("Horizontal_" + this.InputIndex);
                var keyboardY = Input.GetAxis("Vertical_" + this.InputIndex);

                var controllerX = (this.InputIndex % 2 == 0)
                                      ? this.GamePadState.ThumbSticks.Left.X
                                      : this.GamePadState.ThumbSticks.Right.X;
                var controllerY = (this.InputIndex % 2 == 0)
                                      ? this.GamePadState.ThumbSticks.Left.Y
                                      : this.GamePadState.ThumbSticks.Right.Y;

                var x = Mathf.Abs(keyboardX) > 0.01f ? keyboardX : controllerX;
                var y = Mathf.Abs(keyboardY) > 0.01f ? keyboardY : controllerY;

                return new Vector2(x, y).normalized;
            }
        }

        public bool DropItemPressed
        {
            get
            {
                if (!this.AcceptsInput)
                {
                    return false;
                }

                float axis = (this.InputIndex % 2 == 0)
                                 ? this.GamePadState.Triggers.Left
                                 : this.GamePadState.Triggers.Right;

                var input = "DropItem_" + this.InputIndex;
                return Input.GetButtonDown(input) || axis > 0.5f;
            }
        }

        #endregion

        #region Properties

        private bool AcceptsInput
        {
            get
            {
                return this.CurrentMatchState > MatchState.PreMatch;
            }
        }

        private GamePadState GamePadState
        {
            get
            {
                var playerIndex = (PlayerIndex)(this.InputIndex / 2);
                return GamePad.GetState(playerIndex);
            }
        }

        #endregion

        #region Methods

        protected override void RegisterListeners()
        {
            this.RegisterListener(ToyHuntEvent.MatchStateChanged, this.OnMatchStateChanged);
        }

        private void OnMatchStateChanged(Event e)
        {
            this.CurrentMatchState = (MatchState)e.EventData;
        }

        #endregion
    }
}