﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Orientation.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Unity.Rendering
{
    public enum Orientation
    {
        North,

        West,

        East,

        South
    }
}