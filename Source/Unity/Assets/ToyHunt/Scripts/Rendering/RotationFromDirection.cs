﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RotationFromDirection.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Unity.Rendering
{
    using Slash.Collections.AttributeTables;
    using Slash.Math.Utils;
    using Slash.Unity.Common.Core;

    using ToyHunt.Logic.Components;

    using UnityEngine;

    public class RotationFromDirection : MonoBehaviour
    {
        #region Fields

        public Rigidbody2D Rigidbody;

        private EntityConfigurationBehaviour entityConfigurationBehaviour;

        #endregion

        #region Public Properties

        public Orientation CurrentOrientation { get; private set; }

        #endregion

        #region Methods

        private void Awake()
        {
            this.entityConfigurationBehaviour = this.GetComponent<EntityConfigurationBehaviour>();
        }

        /// <summary>
        ///   Per frame update.
        /// </summary>
        private void Update()
        {
            this.UpdateRotation();
        }

        private void UpdateRotation()
        {
            // Get move direction.
            Vector2 direction = this.Rigidbody.velocity.normalized;
            if (direction.sqrMagnitude > 0.01f)
            {
                if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y))
                {
                    // Horizontal movement.
                    this.CurrentOrientation = MathF.Sign(direction.x) < 0 ? Orientation.West : Orientation.East;

                    // Flip if necessary.
                    this.transform.rotation = Quaternion.Euler(0, MathF.Sign(direction.x) < 0 ? 180 : 0, 0);
                }
                else
                {
                    // Vertical movement.
                    this.CurrentOrientation = MathF.Sign(direction.y) < 0 ? Orientation.South : Orientation.North;
                }
            }
            else
            {
                // Rotation from player owner.
                var playerIndex =
                    this.entityConfigurationBehaviour.Configuration.GetIntOrDefault(
                        PlayerOwnerComponent.AttributePlayerOwner, PlayerOwnerComponent.DefaultPlayerOwner);
                this.CurrentOrientation = playerIndex == 0 ? Orientation.East : Orientation.West;
            }
        }

        #endregion
    }
}