﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SortingOrderFromPosition.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Unity.Rendering
{
    using UnityEngine;

    /// <summary>
    ///   Sets the sorting order of the renderer dependent on the Y position of the object.
    /// </summary>
    public class SortingOrderFromPosition : MonoBehaviour
    {
        #region Fields

        public LevelPosition LevelPosition;

        public Renderer Renderer;

        public bool Static;

        #endregion

        #region Methods

        /// <summary>
        ///   Called before first Update call.
        /// </summary>
        private void Start()
        {
            // Update sorting order.
            this.UpdateSortingOrder();
        }

        /// <summary>
        ///   Per frame update.
        /// </summary>
        private void Update()
        {
            if (!this.Static)
            {
                // Update sorting order.
                this.UpdateSortingOrder();
            }
        }

        private void UpdateSortingOrder()
        {
            // Get y position.
            float y = this.LevelPosition.Pivot.y;

            this.Renderer.sortingOrder = -Mathf.FloorToInt(y);
        }

        #endregion
    }
}