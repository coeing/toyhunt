﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LevelPosition.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Unity.Rendering
{
    using UnityEngine;

    public class LevelPosition : MonoBehaviour
    {
        #region Fields

        public Vector2 CachedPivot;

        public Collider2D Collider;

        #endregion

        #region Public Properties

        public Vector2 Pivot
        {
            get
            {
                return this.Collider.transform.position;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        ///   Called before first Update call.
        /// </summary>
        private void Start()
        {
            this.CachedPivot = this.Pivot;
        }

        /// <summary>
        ///   Per frame update.
        /// </summary>
        private void Update()
        {
        }

        #endregion
    }
}