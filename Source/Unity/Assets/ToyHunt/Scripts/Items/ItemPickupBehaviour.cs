﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ItemPickupBehaviour.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Items
{
    using Slash.GameBase;
    using Slash.Unity.Common.Core;

    using ToyHunt.Core.Logic;
    using ToyHunt.Logic.Components;
    using ToyHunt.Logic.Data;
    using ToyHunt.Logic.EventData;

    using UnityEngine;

    using Event = Slash.GameBase.Events.Event;

    public class ItemPickupBehaviour : MonoBehaviour
    {
        #region Fields

        public Collider2D Collider2D;

        /// <summary>
        ///   Force applied to dropped items in order to make them spin and fall to the ground.
        /// </summary>
        public float DropForce;

        /// <summary>
        ///   Factor to apply for drop force when dropping due to being dashed.
        /// </summary>
        public float DropWhenDashedForceFactor = 2.5f;

        /// <summary>
        ///   Time gravity is applied to an item when dropped, and item is prevented from being picked up again, in seconds.
        /// </summary>
        public float FallTime;

        public Renderer Renderer;

        public Rigidbody2D Rigidbody2D;

        private EntityConfigurationBehaviour entityConfigurationBehaviour;

        private float fallTimeRemaining;

        /// <summary>
        ///   Whether gravity is currently being applied to this item and the item can't be picked up right now, or not.
        /// </summary>
        private bool falling;

        private GameBehaviour gameBehaviour;

        private EntityConfigurationBehaviour owner;

        #endregion

        #region Methods

        private void Awake()
        {
            this.gameBehaviour = FindObjectOfType<GameBehaviour>();

            this.gameBehaviour.GameChanged += this.OnGameChanged;
            this.entityConfigurationBehaviour = this.GetComponent<EntityConfigurationBehaviour>();
        }

        private void OnGameChanged(Game newGame, Game oldGame)
        {
            if (oldGame != null)
            {
                oldGame.EventManager.RemoveListener(ToyHuntEvent.ItemPickedUp, this.OnItemPickedUp);
                oldGame.EventManager.RemoveListener(ToyHuntEvent.ItemDropped, this.OnItemDropped);
                oldGame.EventManager.RemoveListener(ToyHuntEvent.ItemScored, this.OnItemScored);
            }

            if (newGame != null)
            {
                newGame.EventManager.RegisterListener(ToyHuntEvent.ItemPickedUp, this.OnItemPickedUp);
                newGame.EventManager.RegisterListener(ToyHuntEvent.ItemDropped, this.OnItemDropped);
                newGame.EventManager.RegisterListener(ToyHuntEvent.ItemScored, this.OnItemScored);
            }
        }

        private void OnItemDropped(Event e)
        {
            var data = (ItemDroppedData)e.EventData;

            if (data.ItemEntityId != this.entityConfigurationBehaviour.EntityId)
            {
                return;
            }

            this.Renderer.enabled = true;
            this.Collider2D.enabled = true;

            this.transform.position = this.owner.transform.position;
            this.owner = null;

            // Check if in target zone.
            float orientation;

            var pawnTargetZoneRelationComponent =
                this.gameBehaviour.Game.EntityManager.GetComponent<PawnTargetZoneRelationComponent>(data.PawnEntityId);

            if (pawnTargetZoneRelationComponent.CurrentTargetZone > 0)
            {
                // Boost towards target zone.
                var targetZoneObject = EntityGameObjectMap.Instance[pawnTargetZoneRelationComponent.CurrentTargetZone];
                var toTargetZone = targetZoneObject.transform.position - this.transform.position;
                orientation = toTargetZone.normalized.x;
            }
            else
            {
                // Boost up.
                orientation = (Random.value * 2) - 1.0f;
            }

            Vector2 direction = new Vector2(orientation, 1);
            Vector2 force = direction * this.DropForce;

            if (data.DropReason == DropReason.DroppedByDashCollision)
            {
                force *= this.DropWhenDashedForceFactor;
            }

            this.Rigidbody2D.AddForce(force, ForceMode2D.Impulse);

            // Fall down.
            this.falling = true;
            this.fallTimeRemaining = this.FallTime;
            this.Rigidbody2D.gravityScale = 100.0f;
        }

        private void OnItemPickedUp(Event e)
        {
            var data = (PawnItemData)e.EventData;

            if (data.ItemEntityId != this.entityConfigurationBehaviour.EntityId)
            {
                return;
            }

            this.Renderer.enabled = false;
            this.Collider2D.enabled = false;

            // Set owner.
            var ownerObject = EntityGameObjectMap.Instance[data.PawnEntityId];
            this.owner = ownerObject.GetComponent<EntityConfigurationBehaviour>();
        }

        private void OnItemScored(Event e)
        {
            var data = (ItemScoredData)e.EventData;

            if (data.ItemEntityId != this.entityConfigurationBehaviour.EntityId)
            {
                return;
            }

            // Scored items can't be picked up again.
            this.Collider2D.enabled = false;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (this.falling)
            {
                return;
            }

            var pawn = other.GetComponentInParent<EntityConfigurationBehaviour>();

            if (this.gameBehaviour.Game != null && pawn != null && pawn.BlueprintId.Equals("Player"))
            {
                var data = new PawnItemData
                    {
                        ItemEntityId = this.entityConfigurationBehaviour.EntityId,
                        PawnEntityId = pawn.EntityId
                    };
                this.gameBehaviour.Game.EventManager.QueueEvent(ToyHuntAction.PickUpItem, data);
            }
        }

        private void Update()
        {
            if (this.falling)
            {
                this.fallTimeRemaining -= Time.deltaTime;

                if (this.fallTimeRemaining <= 0)
                {
                    // Stop falling.
                    this.falling = false;
                    this.Rigidbody2D.gravityScale = 0.0f;
                }
            }
        }

        #endregion
    }
}