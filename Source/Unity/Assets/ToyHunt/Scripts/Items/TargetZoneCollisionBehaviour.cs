﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TargetZoneCollisionBehaviour.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Items
{
    using Slash.Unity.Common.Core;

    using ToyHunt.Core.Logic;
    using ToyHunt.Logic.EventData;

    using UnityEngine;

    public class TargetZoneCollisionBehaviour : MonoBehaviour
    {
        #region Fields

        private EntityConfigurationBehaviour entityConfigurationBehaviour;

        private GameBehaviour gameBehaviour;

        #endregion

        #region Methods

        private void Awake()
        {
            this.gameBehaviour = FindObjectOfType<GameBehaviour>();
            this.entityConfigurationBehaviour = this.GetComponent<EntityConfigurationBehaviour>();
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            var pawn = other.GetComponentInParent<EntityConfigurationBehaviour>();

            if (this.gameBehaviour.Game != null && pawn != null && pawn.BlueprintId.Equals("Player"))
            {
                var data = new TargetZoneEventData
                    {
                        TargetZoneEntityId = this.entityConfigurationBehaviour.EntityId,
                        PawnEntityId = pawn.EntityId
                    };
                this.gameBehaviour.Game.EventManager.QueueEvent(ToyHuntEvent.TargetZoneEntered, data);
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            var pawn = other.GetComponentInParent<EntityConfigurationBehaviour>();

            if (this.gameBehaviour.Game != null && pawn != null && pawn.BlueprintId.Equals("Player"))
            {
                var data = new TargetZoneEventData
                    {
                        TargetZoneEntityId = this.entityConfigurationBehaviour.EntityId,
                        PawnEntityId = pawn.EntityId
                    };
                this.gameBehaviour.Game.EventManager.QueueEvent(ToyHuntEvent.TargetZoneLeft, data);
            }
        }

        #endregion
    }
}