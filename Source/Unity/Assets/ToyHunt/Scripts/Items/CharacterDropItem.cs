﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CharacterDropItem.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Items
{
    using Slash.Unity.Common.Core;

    using ToyHunt.Core.Logic;
    using ToyHunt.Unity.Input;

    using UnityEngine;

    public class CharacterDropItem : MonoBehaviour
    {
        #region Fields

        public CharacterInput CharacterInput;

        private bool dropItemPressedLastFrame;

        private EntityConfigurationBehaviour entityConfigurationBehaviour;

        private GameBehaviour gameBehaviour;

        #endregion

        #region Methods

        private void Awake()
        {
            this.gameBehaviour = FindObjectOfType<GameBehaviour>();
            this.entityConfigurationBehaviour = this.GetComponent<EntityConfigurationBehaviour>();
        }

        private void DropItem()
        {
            if (this.gameBehaviour.Game != null && this.entityConfigurationBehaviour != null)
            {
                this.gameBehaviour.Game.EventManager.QueueEvent(
                    ToyHuntAction.DropItem, this.entityConfigurationBehaviour.EntityId);
            }
        }

        private void Update()
        {
            if (this.CharacterInput.DropItemPressed && !this.dropItemPressedLastFrame)
            {
                this.DropItem();
            }

            this.dropItemPressedLastFrame = this.CharacterInput.DropItemPressed;
        }

        #endregion
    }
}