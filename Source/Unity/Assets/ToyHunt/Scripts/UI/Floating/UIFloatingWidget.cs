﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UIFloatingWidget.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Unity.UI.Floating
{
    using UnityEngine;

    public class UIFloatingWidget : MonoBehaviour
    {
        #region Fields

        public float FadeSpeed = 0.33f;

        public float FloatingSpeed = 1.0f;

        public float Lifetime;

        public UIWidget Widget;

        private float lifetimeRemaining;

        #endregion

        #region Methods

        private void Awake()
        {
            if (this.Widget == null)
            {
                this.Widget = this.GetComponent<UIWidget>();
            }
        }

        private void OnEnable()
        {
            this.lifetimeRemaining = this.Lifetime;
        }

        private void Update()
        {
            this.lifetimeRemaining -= Time.deltaTime;

            if (this.lifetimeRemaining <= 0)
            {
                Destroy(this.gameObject);
            }
            else
            {
                // Float.
                this.transform.localPosition += new Vector3(0.0f, this.FloatingSpeed * Time.deltaTime, 0.0f);

                // Fade.
                this.Widget.color = new Color(
                    this.Widget.color.r,
                    this.Widget.color.g,
                    this.Widget.color.b,
                    this.Widget.color.a - this.FadeSpeed * Time.deltaTime);
            }
        }

        #endregion
    }
}