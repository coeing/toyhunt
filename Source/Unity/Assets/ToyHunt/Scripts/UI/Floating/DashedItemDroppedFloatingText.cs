﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DashedItemDroppedFloatingText.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Unity.UI.Floating
{
    using Slash.Unity.Common.Core;

    using ToyHunt.Core.Logic;
    using ToyHunt.Logic.Data;
    using ToyHunt.Logic.EventData;

    using UnityEngine;

    using Event = Slash.GameBase.Events.Event;

    public class DashedItemDroppedFloatingText : GameEventBehaviour
    {
        #region Fields

        public string Text = "Dashed!";

        public Vector3 TextOffset = new Vector2(0, 50.0f);

        private EntityConfigurationBehaviour entityConfigurationBehaviour;

        private UIFloatingWidgetRoot floatingWidgetRoot;

        #endregion

        #region Methods

        protected override void Awake()
        {
            base.Awake();

            this.floatingWidgetRoot = FindObjectOfType<UIFloatingWidgetRoot>();
            this.entityConfigurationBehaviour = this.GetComponent<EntityConfigurationBehaviour>();
        }

        protected override void RegisterListeners()
        {
            this.RegisterListener(ToyHuntEvent.ItemDropped, this.OnItemDropped);
        }

        private void OnItemDropped(Event e)
        {
            var data = (ItemDroppedData)e.EventData;

            if (data.PawnEntityId != this.entityConfigurationBehaviour.EntityId
                || data.DropReason != DropReason.DroppedByDashCollision)
            {
                return;
            }

            // Show floating text.
            if (this.floatingWidgetRoot != null)
            {
                this.floatingWidgetRoot.CreateLabel(
                    this.gameObject.transform.localPosition / 2 + this.TextOffset, this.Text);
            }
        }

        #endregion
    }
}