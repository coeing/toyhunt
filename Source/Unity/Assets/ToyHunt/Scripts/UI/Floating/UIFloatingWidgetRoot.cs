﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UIFloatingWidgetRoot.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Unity.UI.Floating
{
    using UnityEngine;

    public class UIFloatingWidgetRoot : MonoBehaviour
    {
        #region Fields

        public GameObject FloatingLabelPrefab;

        #endregion

        #region Public Methods and Operators

        public void CreateLabel(Vector2 screenPosition, string text)
        {
            var floatingGameObject = NGUITools.AddChild(this.gameObject, this.FloatingLabelPrefab);
            var label = floatingGameObject.GetComponent<UILabel>();

            floatingGameObject.transform.localPosition = screenPosition;
            label.text = text;
        }

        #endregion
    }
}