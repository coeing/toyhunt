﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EnteredTargetZoneFloatingText.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Unity.UI.Floating
{
    using Slash.Unity.Common.Core;

    using ToyHunt.Core.Logic;
    using ToyHunt.Logic.Components;
    using ToyHunt.Logic.EventData;

    using UnityEngine;

    using Event = Slash.GameBase.Events.Event;

    public class EnteredTargetZoneFloatingText : GameEventBehaviour
    {
        #region Fields

        public string Text = "Drop Now!";

        public Vector3 TextOffset = new Vector2(0, 50.0f);

        private EntityConfigurationBehaviour entityConfigurationBehaviour;

        private UIFloatingWidgetRoot floatingWidgetRoot;

        #endregion

        #region Methods

        protected override void Awake()
        {
            base.Awake();

            this.floatingWidgetRoot = FindObjectOfType<UIFloatingWidgetRoot>();
            this.entityConfigurationBehaviour = this.GetComponent<EntityConfigurationBehaviour>();
        }

        protected override void RegisterListeners()
        {
            this.RegisterListener(ToyHuntEvent.OwnTargetZoneEntered, this.OnOwnTargetZoneEntered);
        }

        private void OnOwnTargetZoneEntered(Event e)
        {
            var data = (TargetZoneEventData)e.EventData;
            var inventoryComponent = this.Game.EntityManager.GetComponent<InventoryComponent>(data.PawnEntityId);

            if (data.PawnEntityId != this.entityConfigurationBehaviour.EntityId || inventoryComponent.Items.Count == 0)
            {
                return;
            }

            // Show floating text.
            if (this.floatingWidgetRoot != null)
            {
                this.floatingWidgetRoot.CreateLabel(
                    this.gameObject.transform.localPosition / 2 + this.TextOffset, this.Text);
            }
        }

        #endregion
    }
}