﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ItemPickedUpFloatingText.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Unity.UI.Floating
{
    using Slash.Unity.Common.Core;

    using ToyHunt.Core.Logic;
    using ToyHunt.Logic.EventData;

    using UnityEngine;

    using Event = Slash.GameBase.Events.Event;

    public class ItemPickedUpFloatingText : GameEventBehaviour
    {
        #region Fields

        public string Text = "Return Item To Score!";

        public Vector3 TextOffset = new Vector2(0, 50.0f);

        private EntityConfigurationBehaviour entityConfigurationBehaviour;

        private UIFloatingWidgetRoot floatingWidgetRoot;

        #endregion

        #region Methods

        protected override void Awake()
        {
            base.Awake();

            this.floatingWidgetRoot = FindObjectOfType<UIFloatingWidgetRoot>();
            this.entityConfigurationBehaviour = this.GetComponent<EntityConfigurationBehaviour>();
        }

        protected override void RegisterListeners()
        {
            this.RegisterListener(ToyHuntEvent.ItemPickedUp, this.OnItemPickedUp);
        }

        private void OnItemPickedUp(Event e)
        {
            if (!this.enabled)
            {
                return;
            }

            var data = (PawnItemData)e.EventData;

            if (data.PawnEntityId != this.entityConfigurationBehaviour.EntityId)
            {
                return;
            }

            // Show floating text.
            if (this.floatingWidgetRoot != null)
            {
                this.floatingWidgetRoot.CreateLabel(
                    this.gameObject.transform.localPosition / 2 + this.TextOffset, this.Text);
            }

            // Show only once.
            this.enabled = false;
        }

        #endregion
    }
}