﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GameContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Unity.UI
{
    using System.Collections.Generic;

    using EZData;

    using Slash.GameBase;
    using Slash.GameBase.Events;
    using Slash.Unity.Common.Core;

    using UnityEngine;

    public class GameContext : Context
    {
        #region Fields

        /// <summary>
        ///   Events to register for at FreudBot games.
        /// </summary>
        private readonly Dictionary<object, EventManager.EventDelegate> events =
            new Dictionary<object, EventManager.EventDelegate>();

        /// <summary>
        ///   Behaviour which manages the game.
        /// </summary>
        private readonly GameBehaviour gameBehaviour;

        #endregion

        #region Constructors and Destructors

        protected GameContext()
        {
            this.gameBehaviour = Object.FindObjectOfType<GameBehaviour>();
            if (this.gameBehaviour != null)
            {
                this.gameBehaviour.GameChanged += this.OnGameChanged;
            }
            else
            {
                Debug.LogError("No game behaviour found");
            }
        }

        #endregion

        #region Properties

        protected Game Game
        {
            get
            {
                return this.gameBehaviour != null ? this.gameBehaviour.Game : null;
            }
        }

        #endregion

        #region Methods

        protected virtual void OnGameChanged(Game newGame, Game oldGame)
        {
            if (oldGame != null)
            {
                foreach (var gameEvent in this.events)
                {
                    oldGame.EventManager.RemoveListener(gameEvent.Key, gameEvent.Value);
                }
            }

            if (newGame != null)
            {
                foreach (var gameEvent in this.events)
                {
                    newGame.EventManager.RegisterListener(gameEvent.Key, gameEvent.Value);
                }
            }
        }

        /// <summary>
        ///   Registers for the specified event at any future FreudBot games that might start.
        /// </summary>
        /// <param name="eventType">Type of the event to register for.</param>
        /// <param name="callback">Method to call when the event is raised.</param>
        protected void RegisterListener(object eventType, EventManager.EventDelegate callback)
        {
            this.events.Add(eventType, callback);
        }

        #endregion
    }
}