﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InGameMenu.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Unity.UI.InGameMenu
{
    using System;

    using Slash.Unity.Common.Scenes;

    using UnityEngine;

    public class InGameMenu : MonoBehaviour
    {
        #region Public Events

        public event Action PlayPressed;

        #endregion

        #region Public Methods and Operators

        public void OnMainMenu()
        {
            SceneManager.Instance.ChangeScene("MainMenu");
        }

        public void OnPlay()
        {
            this.OnPlayPressed();
        }

        #endregion

        #region Methods

        private void OnPlayPressed()
        {
            var handler = this.PlayPressed;
            if (handler != null)
            {
                handler();
            }
        }

        #endregion
    }
}