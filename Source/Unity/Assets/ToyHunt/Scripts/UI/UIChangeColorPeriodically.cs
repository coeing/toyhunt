﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UIChangeColorPeriodically.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Unity.UI
{
    using UnityEngine;

    public class UIChangeColorPeriodically : MonoBehaviour
    {
        #region Fields

        public float ColorDuration = 0.5f;

        public Color[] Colors;

        public UIWidget Target;

        private int currentColor;

        private float currentColorDurationRemaining;

        #endregion

        #region Methods

        private void OnDisable()
        {
            this.currentColor = 0;
            this.UpdateWidgetColor();
        }

        private void OnEnable()
        {
            if (this.Target == null)
            {
                this.Target = this.GetComponent<UIWidget>();
            }

            this.currentColor = 1;
            this.UpdateWidgetColor();
        }

        private void Update()
        {
            this.currentColorDurationRemaining -= Time.deltaTime;

            if (this.currentColorDurationRemaining <= 0)
            {
                this.currentColor = (this.currentColor + 1) % this.Colors.Length;
                this.UpdateWidgetColor();
            }
        }

        private void UpdateWidgetColor()
        {
            this.Target.color = this.Colors[this.currentColor];
            this.currentColorDurationRemaining = this.ColorDuration;
        }

        #endregion
    }
}