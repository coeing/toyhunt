﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Credits.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Unity.UI.Credits
{
    using Slash.Unity.Common.Scenes;

    using UnityEngine;

    public class Credits : MonoBehaviour
    {
        #region Public Methods and Operators

        public void OnBack()
        {
            NguiUtils.DisableInput();
            SceneManager.Instance.ChangeScene("MainMenu", 0.2f);
        }

        public void Update()
        {
            if (Input.GetButtonDown("Back"))
            {
                SceneManager.Instance.ChangeScene("MainMenu");
            }
        }

        #endregion
    }
}