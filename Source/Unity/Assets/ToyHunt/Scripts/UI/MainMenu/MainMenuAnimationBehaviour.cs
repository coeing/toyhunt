﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainMenuAnimationBehaviour.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Unity.UI.MainMenu
{
    using ToyHunt.Unity.Visual;

    using UnityEngine;

    public class MainMenuAnimationBehaviour : MonoBehaviour
    {
        #region Fields

        public SpriteAnimationData[] Animations;

        public SpriteAnimation SpriteAnimation;

        #endregion

        #region Methods

        private void OnAnimationFinished(string obj)
        {
            // Play random animation.
            var index = Random.Range(0, this.Animations.Length);
            this.SpriteAnimation.PlayAnimation(this.Animations[index]);
        }

        private void Start()
        {
            this.SpriteAnimation.AnimationFinished += this.OnAnimationFinished;
        }

        #endregion
    }
}