﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainMenu.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Unity.UI.MainMenu
{
    using Slash.Unity.Common.Scenes;

    using UnityEngine;

    public class MainMenu : MonoBehaviour
    {
        #region Fields

        private SceneManager sceneManager;

        #endregion

        #region Public Methods and Operators

        public void OnCredits()
        {
            NguiUtils.DisableInput();
            this.sceneManager.ChangeScene("Credits", 0.2f);
        }

        public void OnEnd()
        {
            this.sceneManager.Quit();
        }

        public void OnStart()
        {
            NguiUtils.DisableInput();
            this.sceneManager.ChangeScene("GameScene", 0.2f);
        }

        #endregion

        #region Methods

        private void Awake()
        {
            this.sceneManager = SceneManager.Instance;
        }

        #endregion
    }
}