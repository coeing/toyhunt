﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NguiUtils.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Unity.UI
{
    public class NguiUtils
    {
        #region Public Methods and Operators

        public static void DisableInput()
        {
            UICamera.current.useController = false;
            UICamera.current.useKeyboard = false;
            UICamera.current.useMouse = false;
            UICamera.current.useTouch = false;
        }

        #endregion
    }
}