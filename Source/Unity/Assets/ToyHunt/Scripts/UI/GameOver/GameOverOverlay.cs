﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GameOverOverlay.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Unity.UI.GameOver
{
    using Slash.Unity.Common.Scenes;

    using UnityEngine;

    public class GameOverOverlay : MonoBehaviour
    {
        #region Public Methods and Operators

        public void OnMainMenu()
        {
            SceneManager.Instance.ChangeScene("MainMenu");
        }

        public void OnReplay()
        {
            SceneManager.Instance.ChangeScene("GameScene");
        }

        #endregion
    }
}