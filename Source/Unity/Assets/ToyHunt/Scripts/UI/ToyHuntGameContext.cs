﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ToyHuntGameContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Unity.UI
{
    using System;

    using EZData;

    using Slash.GameBase.Events;

    using ToyHunt.Core.Logic;
    using ToyHunt.Logic.Data;
    using ToyHunt.Logic.EventData;

    public class ToyHuntGameContext : GameContext
    {
        #region Fields

        private readonly Property<int> currentMatchStateProperty = new Property<int>();

        private readonly Property<bool> highlightTimerProperty = new Property<bool>();

        private readonly Property<bool> justStartedProperty = new Property<bool>();

        private readonly Property<int> player1ScoreProperty = new Property<int>();

        private readonly Property<int> player2ScoreProperty = new Property<int>();

        private readonly Property<int> remainingMinutesProperty = new Property<int>();

        private readonly Property<int> remainingSecondsProperty = new Property<int>();

        private readonly Property<int> timeRemainingCeilingProperty = new Property<int>();

        private readonly Property<float> timeRemainingProperty = new Property<float>();

        private readonly Property<string> winnerNameProperty = new Property<string>();

        private readonly Property<int> winnerProperty = new Property<int>();

        private float levelTime;

        #endregion

        #region Constructors and Destructors

        public ToyHuntGameContext()
        {
            this.RegisterListener(ToyHuntEvent.PlayerScoreChanged, this.OnPlayerScoreChanged);
            this.RegisterListener(ToyHuntEvent.LevelTimerStarted, this.OnLevelTimerStarted);
            this.RegisterListener(ToyHuntEvent.PreMatchTimerStarted, this.OnPreMatchTimerStarted);
            this.RegisterListener(ToyHuntEvent.MatchStateChanged, this.OnMatchStateChanged);
            this.RegisterListener(ToyHuntEvent.GameOver, this.OnGameOver);
        }

        #endregion

        #region Public Properties

        public int CurrentMatchState
        {
            get
            {
                return this.currentMatchStateProperty.GetValue();
            }
            set
            {
                this.currentMatchStateProperty.SetValue(value);
            }
        }

        public Property<int> CurrentMatchStateProperty
        {
            get
            {
                return this.currentMatchStateProperty;
            }
        }

        public bool HighlightTimer
        {
            get
            {
                return this.highlightTimerProperty.GetValue();
            }
            set
            {
                this.highlightTimerProperty.SetValue(value);
            }
        }

        public Property<bool> HighlightTimerProperty
        {
            get
            {
                return this.highlightTimerProperty;
            }
        }

        public bool JustStarted
        {
            get
            {
                return this.justStartedProperty.GetValue();
            }
            set
            {
                this.justStartedProperty.SetValue(value);
            }
        }

        public Property<bool> JustStartedProperty
        {
            get
            {
                return this.justStartedProperty;
            }
        }

        public int Player1Score
        {
            get
            {
                return this.player1ScoreProperty.GetValue();
            }
            set
            {
                this.player1ScoreProperty.SetValue(value);
            }
        }

        public Property<int> Player1ScoreProperty
        {
            get
            {
                return this.player1ScoreProperty;
            }
        }

        public int Player2Score
        {
            get
            {
                return this.player2ScoreProperty.GetValue();
            }
            set
            {
                this.player2ScoreProperty.SetValue(value);
            }
        }

        public Property<int> Player2ScoreProperty
        {
            get
            {
                return this.player2ScoreProperty;
            }
        }

        public int RemainingMinutes
        {
            get
            {
                return this.remainingMinutesProperty.GetValue();
            }
            set
            {
                this.remainingMinutesProperty.SetValue(value);
            }
        }

        public Property<int> RemainingMinutesProperty
        {
            get
            {
                return this.remainingMinutesProperty;
            }
        }

        public int RemainingSeconds
        {
            get
            {
                return this.remainingSecondsProperty.GetValue();
            }
            set
            {
                this.remainingSecondsProperty.SetValue(value);
            }
        }

        public Property<int> RemainingSecondsProperty
        {
            get
            {
                return this.remainingSecondsProperty;
            }
        }

        public float TimeRemaining
        {
            get
            {
                return this.timeRemainingProperty.GetValue();
            }
            set
            {
                this.timeRemainingProperty.SetValue(value);

                this.TimeRemainingCeiling = (int)Math.Ceiling(this.TimeRemaining);
            }
        }

        public int TimeRemainingCeiling
        {
            get
            {
                return this.timeRemainingCeilingProperty.GetValue();
            }
            set
            {
                this.timeRemainingCeilingProperty.SetValue(value);
            }
        }

        public Property<int> TimeRemainingCeilingProperty
        {
            get
            {
                return this.timeRemainingCeilingProperty;
            }
        }

        public Property<float> TimeRemainingProperty
        {
            get
            {
                return this.timeRemainingProperty;
            }
        }

        public int Winner
        {
            get
            {
                return this.winnerProperty.GetValue();
            }
            set
            {
                this.winnerProperty.SetValue(value);
            }
        }

        public string WinnerName
        {
            get
            {
                return this.winnerNameProperty.GetValue();
            }
            set
            {
                this.winnerNameProperty.SetValue(value);
            }
        }

        public Property<string> WinnerNameProperty
        {
            get
            {
                return this.winnerNameProperty;
            }
        }

        public Property<int> WinnerProperty
        {
            get
            {
                return this.winnerProperty;
            }
        }

        #endregion

        #region Public Methods and Operators

        public void Update(float dt)
        {
            if (this.CurrentMatchState == (int)MatchState.GameOver)
            {
                return;
            }

            this.TimeRemaining -= dt;

            if (this.CurrentMatchState == (int)MatchState.Running)
            {
                this.RemainingMinutes = (int)this.TimeRemaining / 60;
                this.RemainingSeconds = (int)this.TimeRemaining % 60;

                this.HighlightTimer = this.TimeRemaining <= 6.0f;

                if (this.levelTime - this.TimeRemaining > 1.0f)
                {
                    this.JustStarted = false;
                }
            }
        }

        #endregion

        #region Methods

        private void OnGameOver(Event e)
        {
            var data = (GameOverData)e.EventData;

            this.Winner = data.Winner + 1;
            this.WinnerName = data.Winner == 0 ? "TEAM BLUE" : "TEAM RED";
            this.HighlightTimer = false;
        }

        private void OnLevelTimerStarted(Event e)
        {
            this.TimeRemaining = (float)e.EventData;
            this.levelTime = (float)e.EventData;
        }

        private void OnMatchStateChanged(Event e)
        {
            MatchState matchState = (MatchState)e.EventData;
            this.CurrentMatchState = (int)matchState;
            if (matchState == MatchState.Running)
            {
                this.JustStarted = true;
            }
        }

        private void OnPlayerScoreChanged(Event e)
        {
            var data = (PlayerScoreChangedData)e.EventData;

            if (data.PlayerIndex == 0)
            {
                this.Player1Score = data.Score;
            }
            else
            {
                this.Player2Score = data.Score;
            }
        }

        private void OnPreMatchTimerStarted(Event e)
        {
            this.TimeRemaining = (float)e.EventData;
        }

        #endregion
    }
}