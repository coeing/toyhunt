﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ChangeColorOnTextChangeBinding.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Unity.UI
{
    using System.Collections.Generic;

    using UnityEngine;

    [RequireComponent(typeof(UILabel))]
    public class ChangeColorOnTextChangeBinding : NguiTextBinding
    {
        #region Fields

        public List<string> IgnoredValues;

        public Color NewColor;

        public float NewColorDuration;

        private bool colorChanged;

        private UILabel label;

        private float newColorDurationRemaining;

        private Color oldColor;

        #endregion

        #region Public Methods and Operators

        public override void Awake()
        {
            base.Awake();

            this.label = this.GetComponent<UILabel>();
            this.oldColor = this.label.color;
        }

        #endregion

        #region Methods

        protected override void ApplyNewValue(string newValue)
        {
            if (this.IgnoredValues.Contains(newValue))
            {
                return;
            }

            this.newColorDurationRemaining = this.NewColorDuration;
            this.label.color = this.NewColor;
            this.colorChanged = true;
        }

        private void Update()
        {
            if (!this.colorChanged)
            {
                return;
            }

            this.newColorDurationRemaining -= Time.deltaTime;

            if (this.newColorDurationRemaining <= 0)
            {
                this.label.color = this.oldColor;
            }
        }

        #endregion
    }
}