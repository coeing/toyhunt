﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ToyHuntGameViewModel.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Unity.UI
{
    using UnityEngine;

    public class ToyHuntGameViewModel : MonoBehaviour
    {
        #region Fields

        public ToyHuntGameContext Context;

        public NguiRootContext View;

        #endregion

        #region Methods

        private void Awake()
        {
            this.Context = new ToyHuntGameContext();
            this.View.SetContext(this.Context);
        }

        private void Update()
        {
            this.Context.Update(Time.deltaTime);
        }

        #endregion
    }
}