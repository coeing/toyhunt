﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DragFromItemCount.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Unity
{
    using Slash.Unity.Common.Core;

    using ToyHunt.Core.Logic;
    using ToyHunt.Logic.EventData;

    using UnityEngine;

    using Event = Slash.GameBase.Events.Event;

    public class DragFromItemCount : GameEventBehaviour
    {
        #region Fields

        /// <summary>
        ///   Additional drag per item.
        /// </summary>
        public float AdditionalDragPerItem = 4;

        /// <summary>
        ///   Initial drag value.
        /// </summary>
        public float InitialDrag;

        public int ItemCount;

        public Rigidbody2D Rigidbody2D;

        private EntityConfigurationBehaviour entityConfigurationBehaviour;

        #endregion

        #region Methods

        protected override void Awake()
        {
            base.Awake();

            this.entityConfigurationBehaviour = this.GetComponent<EntityConfigurationBehaviour>();
        }

        protected override void RegisterListeners()
        {
            this.RegisterListener(ToyHuntEvent.ItemPickedUp, this.OnItemPickedUp);
            this.RegisterListener(ToyHuntEvent.ItemDropped, this.OnItemDropped);
        }

        private void OnItemDropped(Event e)
        {
            // Check if from entity.
            var itemData = (ItemDroppedData)e.EventData;
            if (itemData.PawnEntityId != this.entityConfigurationBehaviour.EntityId)
            {
                return;
            }

            --this.ItemCount;

            this.UpdateDrag();
        }

        private void OnItemPickedUp(Event e)
        {
            // Check if from entity.
            PawnItemData itemData = (PawnItemData)e.EventData;
            if (itemData.PawnEntityId != this.entityConfigurationBehaviour.EntityId)
            {
                return;
            }

            ++this.ItemCount;

            this.UpdateDrag();
        }

        /// <summary>
        ///   Called before first Update call.
        /// </summary>
        private void Start()
        {
            // Store initial drag.
            this.InitialDrag = this.Rigidbody2D.mass;
        }

        private void UpdateDrag()
        {
            // Update drag.
            this.Rigidbody2D.mass = this.InitialDrag + this.ItemCount * this.AdditionalDragPerItem;
        }

        #endregion
    }
}