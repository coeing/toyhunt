﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CharacterMovement.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Unity
{
    using ToyHunt.Unity.Input;

    using UnityEngine;

    public class CharacterMovement : MonoBehaviour
    {
        #region Fields

        public CharacterInput CharacterInput;

        public float KeyboardForce = 100000;

        public float MaxSpeed = 10.0f;

        public Rigidbody2D Rigidbody2D;

        public float Speed;

        public Vector2 Direction
        {
            get
            {
                return this.rigidbody2D.velocity.normalized;
            }
        }

        #endregion

        #region Methods

        private void FixedUpdate()
        {
            // Get input from the keyboard, with automatic smoothing (GetAxis instead of GetAxisRaw).
            // We always want the movement to be framerate independent, so we multiply by Time.deltaTime.
            var direction = this.CharacterInput.Direction;

            var force = direction * this.KeyboardForce * Time.deltaTime;
            this.Rigidbody2D.AddRelativeForce(force);
            
            this.Speed = this.Rigidbody2D.velocity.magnitude;
        }

        private void Update()
        {
            // Calculate maximum speed.
            this.MaxSpeed = this.KeyboardForce / (this.rigidbody2D.mass * this.rigidbody2D.drag / Time.fixedDeltaTime);
        }

        #endregion
    }
}