﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CharacterDash.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Unity
{
    using ToyHunt.Unity.Input;

    using UnityEngine;

    public class CharacterDash : MonoBehaviour
    {
        #region Fields

        public CharacterInput CharacterInput;

        public CharacterMovement CharacterMovement;

        public float Cooldown = 1.0f;

        public float DashForce = 1000.0f;

        private float remainingCooldown;

        private float remainingDashDuration;

        #endregion

        #region Public Properties

        public bool IsDashing
        {
            get
            {
                return this.remainingDashDuration > 0.0f;
            }
        }

        #endregion

        #region Methods

        private void Dash()
        {
            this.rigidbody2D.AddForce(this.CharacterInput.Direction * this.DashForce, ForceMode2D.Impulse);
            this.remainingDashDuration = this.Cooldown;
            this.remainingCooldown = this.Cooldown;
        }

        /// <summary>
        ///   Per frame update.
        /// </summary>
        private void Update()
        {
            this.remainingCooldown -= Time.deltaTime;
            this.remainingDashDuration -= Time.deltaTime;

            if (this.remainingCooldown <= 0.0f && this.CharacterInput.DashPressed)
            {
                this.Dash();
            }
        }

        #endregion
    }
}