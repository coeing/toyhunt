﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CharacterCollision.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Unity
{
    using Slash.Unity.Common.Core;

    using ToyHunt.Core.Logic;
    using ToyHunt.Logic.EventData;

    using UnityEngine;

    public class CharacterCollision : MonoBehaviour
    {
        #region Fields

        public float CollisionForce = 500;

        public CharacterDash Dash;

        public float DashForceFactor = 1.0f;

        private EntityConfigurationBehaviour entityConfigurationBehaviour;

        private GameBehaviour gameBehaviour;

        #endregion

        #region Methods

        private void Awake()
        {
            this.gameBehaviour = FindObjectOfType<GameBehaviour>();
            this.entityConfigurationBehaviour = this.GetComponent<EntityConfigurationBehaviour>();
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            // Check if character is dashing.
            if (!this.Dash.IsDashing)
            {
                return;
            }

            if (collision.gameObject.rigidbody2D != null)
            {
                // Add force to be pushed away.
                collision.gameObject.rigidbody2D.AddForce(
                    collision.relativeVelocity * this.DashForceFactor, ForceMode2D.Impulse);
            }

            // Notify listeners.
            if (this.gameBehaviour.Game != null)
            {
                EntityConfigurationBehaviour collisionEntity =
                    collision.gameObject.GetComponentInParent<EntityConfigurationBehaviour>();
                var data = new DashCollisionData
                    {
                        DashingEntityId =
                            this.entityConfigurationBehaviour != null ? this.entityConfigurationBehaviour.EntityId : 0,
                        DashedEntityId = collisionEntity != null ? collisionEntity.EntityId : 0,
                        Force = collision.relativeVelocity.magnitude
                    };
                this.gameBehaviour.Game.EventManager.QueueEvent(ToyHuntEvent.DashCollision, data);
            }
        }

        #endregion
    }
}