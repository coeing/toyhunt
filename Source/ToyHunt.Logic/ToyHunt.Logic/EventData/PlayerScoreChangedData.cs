﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerScoreChangedData.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace ToyHunt.Logic.EventData
{
    public class PlayerScoreChangedData
    {
        public int PlayerIndex { get; set; }

        public int Score { get; set; }

        public override string ToString()
        {
            return string.Format("PlayerIndex: {0}, Score: {1}", this.PlayerIndex, this.Score);
        }
    }
}