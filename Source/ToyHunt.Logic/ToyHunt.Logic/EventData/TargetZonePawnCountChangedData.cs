﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TargetZonePawnCountChangedData.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Logic.EventData
{
    public class TargetZonePawnCountChangedData
    {
        #region Public Properties

        public int PawnCount { get; set; }

        public int TargetZoneEntityId { get; set; }

        #endregion

        #region Public Methods and Operators

        public override string ToString()
        {
            return string.Format("TargetZoneEntityId: {0}, PawnCount: {1}", this.TargetZoneEntityId, this.PawnCount);
        }

        #endregion
    }
}