﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DashCollisionData.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Logic.EventData
{
    public class DashCollisionData
    {
        #region Public Properties

        public int DashedEntityId { get; set; }

        public int DashingEntityId { get; set; }

        public float Force { get; set; }

        #endregion

        #region Public Methods and Operators

        public override string ToString()
        {
            return string.Format(
                "DashedEntityId: {0}, DashingEntityId: {1}, Force: {2}",
                this.DashedEntityId,
                this.DashingEntityId,
                this.Force);
        }

        #endregion
    }
}