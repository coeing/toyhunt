﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TargetZoneEventData.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Logic.EventData
{
    public class TargetZoneEventData
    {
        #region Public Properties

        public int PawnEntityId { get; set; }

        public int TargetZoneEntityId { get; set; }

        #endregion

        #region Public Methods and Operators

        public override string ToString()
        {
            return string.Format(
                "PawnEntityId: {0}, TargetZoneEntityId: {1}", this.PawnEntityId, this.TargetZoneEntityId);
        }

        #endregion
    }
}