﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ItemScoredData.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Logic.EventData
{
    public class ItemScoredData
    {
        #region Public Properties

        public int ItemEntityId { get; set; }

        public int PlayerIndex { get; set; }

        public int ScoringPawnEntityId { get; set; }

        #endregion

        #region Public Methods and Operators

        public override string ToString()
        {
            return string.Format(
                "ItemEntityId: {0}, PlayerIndex: {1}, ScoringPawnentityId: {2}",
                this.ItemEntityId,
                this.PlayerIndex,
                this.ScoringPawnEntityId);
        }

        #endregion
    }
}