﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ItemDroppedData.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Logic.EventData
{
    using ToyHunt.Logic.Data;

    public class ItemDroppedData
    {
        #region Public Properties

        public DropReason DropReason { get; set; }

        public int ItemEntityId { get; set; }

        public int PawnEntityId { get; set; }

        #endregion

        #region Public Methods and Operators

        public override string ToString()
        {
            return string.Format(
                "ItemEntityId: {0}, PawnEntityId: {1}, DropReason: {2}",
                this.ItemEntityId,
                this.PawnEntityId,
                this.DropReason);
        }

        #endregion
    }
}