﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GameOverData.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace ToyHunt.Logic.EventData
{
    using Slash.Collections.Utils;

    public class GameOverData
    {
        // Player index of the winning player, -1 indicates a draw.
        public int Winner { get; set; }

        public int[] PlayerScores { get; set; }

        public override string ToString()
        {
            return string.Format("Winner: {0}, PlayerScores: {1}", this.Winner, CollectionUtils.ToString(this.PlayerScores));
        }
    }
}