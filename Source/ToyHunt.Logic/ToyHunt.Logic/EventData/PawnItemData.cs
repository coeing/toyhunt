﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ItemOwnerChangedData.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Logic.EventData
{
    public class PawnItemData
    {
        #region Public Properties

        public int ItemEntityId { get; set; }

        public int PawnEntityId { get; set; }

        #endregion

        #region Public Methods and Operators

        public override string ToString()
        {
            return string.Format("ItemEntityId: {0}, PawnEntityId: {1}", this.ItemEntityId, this.PawnEntityId);
        }

        #endregion
    }
}