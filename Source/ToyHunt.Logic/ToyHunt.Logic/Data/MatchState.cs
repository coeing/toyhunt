﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchState.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Logic.Data
{
    public enum MatchState
    {
        Invalid,

        PreMatch,

        Running,

        GameOver
    }
}