﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DropReason.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Logic.Data
{
    public enum DropReason
    {
        DroppedByPlayer,

        DroppedByDashCollision
    }
}