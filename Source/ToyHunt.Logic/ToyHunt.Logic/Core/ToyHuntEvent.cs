﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ToyHuntEvent.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace ToyHunt.Core.Logic
{
    using ToyHunt.Logic.Data;
    using ToyHunt.Logic.EventData;

    public enum ToyHuntEvent
    {
        /// <summary>
        /// Event data: <see cref="ItemScoredData"/>
        /// </summary>
        ItemScored,

        PlayerScoreChanged,

        TimeLimitHit,

        GameOver,

        /// <summary>
        /// Event data: <see cref="float"/> (Level time, in seconds).
        /// </summary>
        LevelTimerStarted,

        /// <summary>
        /// Event data: <see cref="PawnItemData" />
        /// </summary>
        ItemPickedUp,

        /// <summary>
        /// Event data: <see cref="ItemDroppedData" />
        /// </summary>
        ItemDropped,

        /// <summary>
        /// Event data: <see cref="TargetZoneEventData" />
        /// </summary>
        TargetZoneEntered,

        /// <summary>
        /// Event data: <see cref="TargetZoneEventData" />
        /// </summary>
        TargetZoneLeft,

        /// <summary>
        /// Event data: <see cref="DashCollisionData" />
        /// </summary>
        DashCollision,

        AllItemsScored,

        /// <summary>
        /// Event data: <see cref="float"/> (Pre-match time, in seconds).
        /// </summary>
        PreMatchTimerStarted,

        /// <summary>
        /// Event data: <see cref="MatchState"/>
        /// </summary>
        MatchStateChanged,

        /// <summary>
        /// Event data: <see cref="TargetZonePawnCountChangedData"/>
        /// </summary>
        TargetZonePawnCountChanged,

        /// <summary>
        /// Event data: <see cref="TargetZoneEventData" />
        /// </summary>
        OwnTargetZoneEntered
    }
}