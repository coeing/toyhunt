﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ToyHuntAction.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace ToyHunt.Core.Logic
{
    using ToyHunt.Logic.EventData;

    public enum ToyHuntAction
    {
        /// <summary>
        /// Event data: <see cref="PawnItemData" />
        /// </summary>
        PickUpItem,

        /// <summary>
        /// Event data: <see cref="int" /> (Dropping pawn entity id).
        /// </summary>
        DropItem
    }
}