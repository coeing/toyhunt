﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InventorySystem.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Logic.Systems
{
    using System;

    using Slash.Collections.AttributeTables;
    using Slash.GameBase.Events;
    using Slash.GameBase.Systems;

    using ToyHunt.Core.Logic;
    using ToyHunt.Logic.Components;
    using ToyHunt.Logic.Data;
    using ToyHunt.Logic.EventData;

    [GameSystem]
    public class InventorySystem : SystemBase
    {
        #region Public Methods and Operators

        public override void Init(IAttributeTable configuration)
        {
            base.Init(configuration);

            this.Game.EventManager.RegisterListener(ToyHuntEvent.ItemPickedUp, this.OnItemPickedUp);
            this.Game.EventManager.RegisterListener(ToyHuntAction.DropItem, this.OnDropItem);
            this.Game.EventManager.RegisterListener(ToyHuntEvent.DashCollision, this.OnDashCollision);
        }

        #endregion

        #region Methods

        private void OnDashCollision(Event e)
        {
            var data = (DashCollisionData)e.EventData;

            var dashingOwner = data.DashingEntityId != 0
                                   ? this.Game.EntityManager.GetComponent<PlayerOwnerComponent>(data.DashingEntityId)
                                   : null;
            var dashedOwner = data.DashedEntityId != 0
                                  ? this.Game.EntityManager.GetComponent<PlayerOwnerComponent>(data.DashedEntityId)
                                  : null;
            if (dashingOwner == null || dashedOwner == null)
            {
                return;
            }

            if (dashingOwner.PlayerOwner == dashedOwner.PlayerOwner)
            {
                // Don't drop items if both entities belong to the same player.
                return;
            }

            // Drop inventory without scoring points.
            var inventoryComponent = this.Game.EntityManager.GetComponent<InventoryComponent>(data.DashedEntityId);

            while (inventoryComponent.Items.Count > 0)
            {
                // Drop last item.
                var itemEntityId = inventoryComponent.Items.Pop();

                this.Game.EventManager.QueueEvent(
                    ToyHuntEvent.ItemDropped,
                    new ItemDroppedData
                        {
                            ItemEntityId = itemEntityId,
                            PawnEntityId = data.DashedEntityId,
                            DropReason = DropReason.DroppedByDashCollision
                        });
            }
        }

        private void OnDropItem(Event e)
        {
            try
            {
                var pawnEntityId = (int)e.EventData;
                var inventoryComponent = this.Game.EntityManager.GetComponent<InventoryComponent>(pawnEntityId);

                // Check for available items.
                if (inventoryComponent.Items.Count <= 0)
                {
                    return;
                }

                // Drop last item.
                var itemEntityId = inventoryComponent.Items.Pop();

                this.Game.EventManager.QueueEvent(
                    ToyHuntEvent.ItemDropped,
                    new ItemDroppedData
                        {
                            ItemEntityId = itemEntityId,
                            PawnEntityId = pawnEntityId,
                            DropReason = DropReason.DroppedByPlayer
                        });

                // Check for target zone.
                var pawnTargetZoneRelationComponent =
                    this.Game.EntityManager.GetComponent<PawnTargetZoneRelationComponent>(pawnEntityId);

                if (pawnTargetZoneRelationComponent.CurrentTargetZone > 0)
                {
                    // Check owner of target zone.
                    var targetZoneOwnerComponent =
                        this.Game.EntityManager.GetComponent<PlayerOwnerComponent>(
                            pawnTargetZoneRelationComponent.CurrentTargetZone);
                    var pawnOwnerComponent = this.Game.EntityManager.GetComponent<PlayerOwnerComponent>(pawnEntityId);

                    if (targetZoneOwnerComponent.PlayerOwner == pawnOwnerComponent.PlayerOwner)
                    {
                        // Score item.
                        this.Game.EventManager.QueueEvent(
                            ToyHuntEvent.ItemScored,
                            new ItemScoredData
                                {
                                    ItemEntityId = itemEntityId,
                                    PlayerIndex = pawnOwnerComponent.PlayerOwner,
                                    ScoringPawnEntityId = pawnEntityId
                                });
                    }
                }
            }
            catch (Exception exception)
            {
                // HACK(np): Seriously need to take a look at this!
                this.Game.Log.Error(exception.StackTrace);
            }
        }

        private void OnItemPickedUp(Event e)
        {
            var data = (PawnItemData)e.EventData;
            var inventoryComponent = this.Game.EntityManager.GetComponent<InventoryComponent>(data.PawnEntityId);
            inventoryComponent.Items.Push(data.ItemEntityId);
        }

        #endregion
    }
}