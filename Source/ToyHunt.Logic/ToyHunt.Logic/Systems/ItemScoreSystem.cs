﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ItemScoreSystem.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Logic.Systems
{
    using Slash.Collections.AttributeTables;
    using Slash.GameBase.Events;
    using Slash.GameBase.Systems;

    using ToyHunt.Core.Logic;
    using ToyHunt.Logic.Components;
    using ToyHunt.Logic.EventData;

    [GameSystem]
    public class ItemScoreSystem : SystemBase
    {
        #region Fields

        private int[] playerScores;

        private int scoredItems;

        private int totalItems;

        #endregion

        #region Public Methods and Operators

        public override void Init(IAttributeTable configuration)
        {
            base.Init(configuration);

            this.playerScores = new int[2];

            this.Game.EventManager.RegisterListener(FrameworkEventType.EntityInitialized, this.OnEntityInitialized);
            this.Game.EventManager.RegisterListener(ToyHuntEvent.ItemScored, this.OnItemScored);
        }

        #endregion

        #region Methods

        private void OnEntityInitialized(Event e)
        {
            var entityId = (int)e.EventData;
            var itemComponent = this.Game.EntityManager.GetComponent<ItemComponent>(entityId);

            if (itemComponent != null)
            {
                this.totalItems++;
            }
        }

        private void OnItemScored(Event e)
        {
            var data = (ItemScoredData)e.EventData;
            var itemComponent = this.Game.EntityManager.GetComponent<ItemComponent>(data.ItemEntityId);

            // Change score.
            this.playerScores[data.PlayerIndex] += itemComponent.ItemValue;

            // Notify listeners.
            var scoreChangedData = new PlayerScoreChangedData
                {
                    PlayerIndex = data.PlayerIndex,
                    Score = this.playerScores[data.PlayerIndex]
                };
            this.Game.EventManager.QueueEvent(ToyHuntEvent.PlayerScoreChanged, scoreChangedData);

            // Check item count.
            this.scoredItems++;

            if (this.scoredItems >= this.totalItems)
            {
                this.Game.EventManager.QueueEvent(ToyHuntEvent.AllItemsScored);
            }
        }

        #endregion
    }
}