﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ItemPickupSystem.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace ToyHunt.Logic.Systems
{
    using Slash.GameBase.Events;
    using Slash.GameBase.Systems;

    using ToyHunt.Core.Logic;
    using ToyHunt.Logic.EventData;

    [GameSystem]
    public class ItemPickupSystem : SystemBase
    {
        public override void Init(Slash.Collections.AttributeTables.IAttributeTable configuration)
        {
            base.Init(configuration);

            this.Game.EventManager.RegisterListener(ToyHuntAction.PickUpItem, this.OnPickUpItem);
        }

        private void OnPickUpItem(Event e)
        {
            var data = (PawnItemData)e.EventData;
            this.Game.EventManager.QueueEvent(ToyHuntEvent.ItemPickedUp, data);
        }
    }
}