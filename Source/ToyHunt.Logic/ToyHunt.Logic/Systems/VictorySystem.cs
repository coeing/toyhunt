﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VictorySystem.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Logic.Systems
{
    using Slash.Collections.AttributeTables;
    using Slash.GameBase.Events;
    using Slash.GameBase.Systems;

    using ToyHunt.Core.Logic;
    using ToyHunt.Logic.EventData;

    [GameSystem]
    public class VictorySystem : SystemBase
    {
        #region Fields

        public int[] playerScores;

        #endregion

        #region Public Methods and Operators

        public override void Init(IAttributeTable configuration)
        {
            base.Init(configuration);

            this.playerScores = new int[2];

            this.Game.EventManager.RegisterListener(ToyHuntEvent.PlayerScoreChanged, this.OnPlayerScoreChanged);
            this.Game.EventManager.RegisterListener(ToyHuntEvent.TimeLimitHit, this.OnTimeLimitHit);
            this.Game.EventManager.RegisterListener(ToyHuntEvent.AllItemsScored, this.OnAllItemsScored);
        }

        #endregion

        #region Methods

        private void CheckVictory()
        {
            int winner;

            if (this.playerScores[0] > this.playerScores[1])
            {
                winner = 0;
            }
            else if (this.playerScores[1] > this.playerScores[0])
            {
                winner = 1;
            }
            else
            {
                // Draw.
                winner = -1;
            }

            // Notify listeners.
            var data = new GameOverData { PlayerScores = this.playerScores, Winner = winner };

            this.Game.EventManager.QueueEvent(ToyHuntEvent.GameOver, data);
        }

        private void OnAllItemsScored(Event e)
        {
            this.CheckVictory();
        }

        private void OnPlayerScoreChanged(Event e)
        {
            var data = (PlayerScoreChangedData)e.EventData;
            this.playerScores[data.PlayerIndex] = data.Score;
        }

        private void OnTimeLimitHit(Event e)
        {
            this.CheckVictory();
        }

        #endregion
    }
}