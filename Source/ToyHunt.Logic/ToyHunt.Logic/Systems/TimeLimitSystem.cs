﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TimeLimitSystem.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Logic.Systems
{
    using Slash.Collections.AttributeTables;
    using Slash.GameBase.Events;
    using Slash.GameBase.Inspector.Attributes;
    using Slash.GameBase.Systems;

    using ToyHunt.Core.Logic;
    using ToyHunt.Logic.Data;

    [InspectorType]
    [GameSystem]
    public class TimeLimitSystem : SystemBase
    {
        #region Constants

        /// <summary>
        ///   Attribute: Time before the game starts, in seconds.
        /// </summary>
        public const string AttributePreMatchTime = "TimeLimitSystem.PreMatchTime";

        /// <summary>
        ///   Attribute: Match time limit, in seconds.
        /// </summary>
        public const string AttributeTimeLimit = "TimeLimitSystem.TimeLimit";

        /// <summary>
        ///   Attribute default: Time before the game starts, in seconds.
        /// </summary>
        public const float DefaultPreMatchTime = 3.0f;

        /// <summary>
        ///   Attribute default: Match time limit, in seconds.
        /// </summary>
        public const float DefaultTimeLimit = 60.0f;

        #endregion

        #region Fields

        private MatchState matchState = MatchState.Invalid;

        private float timeRemaining;

        #endregion

        #region Public Properties

        /// <summary>
        ///   Time before the game starts, in seconds.
        /// </summary>
        [InspectorFloat(AttributePreMatchTime, Default = DefaultPreMatchTime,
            Description = "Time before the game starts, in seconds.")]
        public float PreMatchTime { get; set; }

        /// <summary>
        ///   Match time limit, in seconds.
        /// </summary>
        [InspectorFloat(AttributeTimeLimit, Default = DefaultTimeLimit, Description = "Match time limit, in seconds.")]
        public float TimeLimit { get; set; }

        #endregion

        #region Public Methods and Operators

        public override void Init(IAttributeTable configuration)
        {
            base.Init(configuration);

            // Load configuration.
            this.TimeLimit = configuration.GetFloatOrDefault(AttributeTimeLimit, DefaultTimeLimit);
            this.PreMatchTime = configuration.GetFloatOrDefault(AttributePreMatchTime, DefaultPreMatchTime);

            // Register listeners.
            this.Game.EventManager.RegisterListener(ToyHuntEvent.AllItemsScored, this.OnAllItemsScored);

            // Start timer.
            this.NextMatchState();
        }

        public override void UpdateSystem(float dt)
        {
            base.UpdateSystem(dt);

            if (this.matchState == MatchState.GameOver)
            {
                return;
            }

            this.timeRemaining -= dt;

            if (this.timeRemaining <= 0)
            {
                this.NextMatchState();
            }
        }

        #endregion

        #region Methods

        private void NextMatchState()
        {
            switch (this.matchState)
            {
                case MatchState.Invalid:
                    // Start pre-match timer.
                    this.timeRemaining = this.PreMatchTime;
                    this.matchState = MatchState.PreMatch;
                    this.Game.EventManager.QueueEvent(ToyHuntEvent.PreMatchTimerStarted, this.timeRemaining);
                    break;

                case MatchState.PreMatch:
                    // Start match timer.
                    this.timeRemaining = this.TimeLimit;
                    this.matchState = MatchState.Running;
                    this.Game.EventManager.QueueEvent(ToyHuntEvent.LevelTimerStarted, this.timeRemaining);
                    break;

                case MatchState.Running:
                    // Stop timer.
                    this.matchState = MatchState.GameOver;
                    this.Game.EventManager.QueueEvent(ToyHuntEvent.TimeLimitHit);
                    break;
            }

            this.Game.EventManager.QueueEvent(ToyHuntEvent.MatchStateChanged, this.matchState);
        }

        private void OnAllItemsScored(Event e)
        {
            this.NextMatchState();
        }

        #endregion
    }
}