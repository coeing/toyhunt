﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TargetZoneSystem.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Logic.Systems
{
    using Slash.Collections.AttributeTables;
    using Slash.GameBase.Events;
    using Slash.GameBase.Systems;

    using ToyHunt.Core.Logic;
    using ToyHunt.Logic.Components;
    using ToyHunt.Logic.EventData;

    [GameSystem]
    public class TargetZoneSystem : SystemBase
    {
        #region Public Methods and Operators

        public override void Init(IAttributeTable configuration)
        {
            base.Init(configuration);

            this.Game.EventManager.RegisterListener(ToyHuntEvent.TargetZoneEntered, this.OnTargetZoneEntered);
            this.Game.EventManager.RegisterListener(ToyHuntEvent.TargetZoneLeft, this.OnTargetZoneLeft);
        }

        #endregion

        #region Methods

        private void OnTargetZoneEntered(Event e)
        {
            var data = (TargetZoneEventData)e.EventData;

            // Update pawn.
            var pawnTargetZoneRelationComponent =
                this.Game.EntityManager.GetComponent<PawnTargetZoneRelationComponent>(data.PawnEntityId);
            pawnTargetZoneRelationComponent.CurrentTargetZone = data.TargetZoneEntityId;

            // Update target zone.
            var targetZoneComponent = this.Game.EntityManager.GetComponent<TargetZoneComponent>(data.TargetZoneEntityId);
            var targetZoneOwner = this.Game.EntityManager.GetComponent<PlayerOwnerComponent>(data.TargetZoneEntityId);
            var pawnOwner = this.Game.EntityManager.GetComponent<PlayerOwnerComponent>(data.PawnEntityId);

            if (targetZoneOwner.PlayerOwner == pawnOwner.PlayerOwner)
            {
                ++targetZoneComponent.PawnCount;

                // Notify listeners.
                var targetZonePawnCountChangedData = new TargetZonePawnCountChangedData
                    {
                        PawnCount = targetZoneComponent.PawnCount,
                        TargetZoneEntityId = data.TargetZoneEntityId
                    };
                this.Game.EventManager.QueueEvent(ToyHuntEvent.TargetZonePawnCountChanged, targetZonePawnCountChangedData);
                this.Game.EventManager.QueueEvent(ToyHuntEvent.OwnTargetZoneEntered, data);
            }
        }

        private void OnTargetZoneLeft(Event e)
        {
            var data = (TargetZoneEventData)e.EventData;

            // Update pawn.
            var pawnTargetZoneRelationComponent =
                this.Game.EntityManager.GetComponent<PawnTargetZoneRelationComponent>(data.PawnEntityId);
            pawnTargetZoneRelationComponent.CurrentTargetZone = -1;

            // Update target zone.
            var targetZoneComponent = this.Game.EntityManager.GetComponent<TargetZoneComponent>(data.TargetZoneEntityId);
            var targetZoneOwner = this.Game.EntityManager.GetComponent<PlayerOwnerComponent>(data.TargetZoneEntityId);
            var pawnOwner = this.Game.EntityManager.GetComponent<PlayerOwnerComponent>(data.PawnEntityId);

            if (targetZoneOwner.PlayerOwner == pawnOwner.PlayerOwner)
            {
                --targetZoneComponent.PawnCount;

                // Notify listeners.
                var targetZonePawnCountChangedData = new TargetZonePawnCountChangedData
                {
                    PawnCount = targetZoneComponent.PawnCount,
                    TargetZoneEntityId = data.TargetZoneEntityId
                };
                this.Game.EventManager.QueueEvent(ToyHuntEvent.TargetZonePawnCountChanged, targetZonePawnCountChangedData);
            }
        }

        #endregion
    }
}