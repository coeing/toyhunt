﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerPawnComponent.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Logic.Components
{
    using Slash.Collections.AttributeTables;
    using Slash.GameBase.Components;
    using Slash.GameBase.Inspector.Attributes;

    [InspectorComponent]
    public class PlayerPawnComponent : IEntityComponent
    {
        #region Constants

        /// <summary>
        ///   Attribute: Index of this pawn, unique among all pawns owned by the same player.
        /// </summary>
        public const string AttributePawnIndex = "PlayerPawnComponent.PawnIndex";

        /// <summary>
        ///   Attribute default: Index of this pawn, unique among all pawns owned by the same player.
        /// </summary>
        public const int DefaultPawnIndex = 0;

        #endregion

        #region Constructors and Destructors

        public PlayerPawnComponent()
        {
            this.PawnIndex = DefaultPawnIndex;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Index of this pawn, unique among all pawns owned by the same player.
        /// </summary>
        [InspectorInt(AttributePawnIndex, Default = DefaultPawnIndex,
            Description = "Index of this pawn, unique among all pawns owned by the same player.")]
        public int PawnIndex { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///   Initializes this component with the data stored in the specified
        ///   attribute table.
        /// </summary>
        /// <param name="attributeTable">Component data.</param>
        public void InitComponent(IAttributeTable attributeTable)
        {
            this.PawnIndex = attributeTable.GetIntOrDefault(AttributePawnIndex, DefaultPawnIndex);
        }

        #endregion
    }
}