﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerOwnerComponent.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Logic.Components
{
    using Slash.Collections.AttributeTables;
    using Slash.GameBase.Components;
    using Slash.GameBase.Inspector.Attributes;

    [InspectorComponent]
    public class PlayerOwnerComponent : IEntityComponent
    {
        #region Constants

        /// <summary>
        ///   Attribute: Index of the player owning this entity.
        /// </summary>
        public const string AttributePlayerOwner = "PlayerOwnerComponent.PlayerOwner";

        /// <summary>
        ///   Attribute default: Index of the player owning this entity.
        /// </summary>
        public const int DefaultPlayerOwner = 0;

        #endregion

        #region Constructors and Destructors

        public PlayerOwnerComponent()
        {
            this.PlayerOwner = DefaultPlayerOwner;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Index of the player owning this entity.
        /// </summary>
        [InspectorInt(AttributePlayerOwner, Default = DefaultPlayerOwner,
            Description = "Index of the player owning this entity.")]
        public int PlayerOwner { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///   Initializes this component with the data stored in the specified
        ///   attribute table.
        /// </summary>
        /// <param name="attributeTable">Component data.</param>
        public void InitComponent(IAttributeTable attributeTable)
        {
            this.PlayerOwner = attributeTable.GetIntOrDefault(AttributePlayerOwner, DefaultPlayerOwner);
        }

        #endregion
    }
}