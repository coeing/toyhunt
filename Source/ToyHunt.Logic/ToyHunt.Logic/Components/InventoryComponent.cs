﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InventoryComponent.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Logic.Components
{
    using System.Collections.Generic;

    using Slash.Collections.AttributeTables;
    using Slash.GameBase.Components;
    using Slash.GameBase.Inspector.Attributes;

    [InspectorComponent]
    public class InventoryComponent : IEntityComponent
    {
        #region Constants

        /// <summary>
        ///   Attribute: Ids of the items carried by this entity.
        /// </summary>
        public const string AttributeItems = "InventoryComponent.Items";

        #endregion

        #region Constructors and Destructors

        public InventoryComponent()
        {
            this.Items = new Stack<int>();
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Ids of the items carried by this entity.
        /// </summary>
        public Stack<int> Items { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///   Initializes this component with the data stored in the specified
        ///   attribute table.
        /// </summary>
        /// <param name="attributeTable">Component data.</param>
        public void InitComponent(IAttributeTable attributeTable)
        {
        }

        #endregion
    }
}