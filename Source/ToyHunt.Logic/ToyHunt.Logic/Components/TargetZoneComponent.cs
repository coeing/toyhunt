﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TargetZoneComponent.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Logic.Components
{
    using Slash.Collections.AttributeTables;
    using Slash.GameBase.Components;
    using Slash.GameBase.Inspector.Attributes;

    [InspectorComponent]
    public class TargetZoneComponent : IEntityComponent
    {
        #region Constants

        /// <summary>
        ///   Attribute: Number of friendly pawns currently in this target zone.
        /// </summary>
        public const string AttributePawnCount = "TargetZoneComponent.PawnCount";

        /// <summary>
        ///   Attribute default: Number of friendly pawns currently in this target zone.
        /// </summary>
        public const int DefaultPawnCount = 0;

        #endregion

        #region Constructors and Destructors

        public TargetZoneComponent()
        {
            this.PawnCount = DefaultPawnCount;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Number of friendly pawns currently in this target zone.
        /// </summary>
        public int PawnCount { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///   Initializes this component with the data stored in the specified
        ///   attribute table.
        /// </summary>
        /// <param name="attributeTable">Component data.</param>
        public void InitComponent(IAttributeTable attributeTable)
        {
            this.PawnCount = attributeTable.GetIntOrDefault(AttributePawnCount, DefaultPawnCount);
        }

        #endregion
    }
}