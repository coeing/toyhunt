﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PawnTargetZoneRelationComponent.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ToyHunt.Logic.Components
{
    using Slash.Collections.AttributeTables;
    using Slash.GameBase.Components;
    using Slash.GameBase.Inspector.Attributes;

    [InspectorComponent]
    public class PawnTargetZoneRelationComponent : IEntityComponent
    {
        #region Constants

        /// <summary>
        ///   Attribute: Entity id of the target zone this entity is currently in, if any.
        /// </summary>
        public const string AttributeCurrentTargetZone = "PawnTargetZoneRelationComponent.CurrentTargetZone";

        /// <summary>
        ///   Attribute default: Entity id of the target zone this entity is currently in, if any.
        /// </summary>
        public const int DefaultCurrentTargetZone = 0;

        #endregion

        #region Constructors and Destructors

        public PawnTargetZoneRelationComponent()
        {
            this.CurrentTargetZone = DefaultCurrentTargetZone;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Entity id of the target zone this entity is currently in, if any.
        /// </summary>
        public int CurrentTargetZone { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///   Initializes this component with the data stored in the specified
        ///   attribute table.
        /// </summary>
        /// <param name="attributeTable">Component data.</param>
        public void InitComponent(IAttributeTable attributeTable)
        {
            this.CurrentTargetZone = attributeTable.GetIntOrDefault(
                AttributeCurrentTargetZone, DefaultCurrentTargetZone);
        }

        #endregion
    }
}