﻿namespace ToyHunt.Logic.Components
{
    using Slash.Collections.AttributeTables;
    using Slash.GameBase.Components;
    using Slash.GameBase.Inspector.Attributes;

    [InspectorComponent]
    public class ItemComponent : IEntityComponent
    {
        /// <summary>
        ///   Attribute: Points scored for returning this item.
        /// </summary>
        public const string AttributeItemValue = "ItemComponent.ItemValue";

        /// <summary>
        ///   Attribute default: Points scored for returning this item.
        /// </summary>
        public const int DefaultItemValue = 1;

        public ItemComponent()
        {
            this.ItemValue = DefaultItemValue;
        }

        /// <summary>
        ///   Points scored for returning this item.
        /// </summary>
        [InspectorInt(AttributeItemValue, Default = DefaultItemValue, Description = "Points scored for returning this item.")]
        public int ItemValue { get; set; }

        /// <summary>
        ///   Initializes this component with the data stored in the specified
        ///   attribute table.
        /// </summary>
        /// <param name="attributeTable">Component data.</param>
        public void InitComponent(IAttributeTable attributeTable)
        {
            this.ItemValue = attributeTable.GetIntOrDefault(AttributeItemValue, DefaultItemValue);
        }
    }
}